<footer class="gj-footer-wrap">
    <address>
        <span>Email: <a href="mailto:gonnajoong@gmail.com">gonnajoong@gmail.com</a></span><br/>
        <span>Phone: <a href="tel:01032906667">010-3290-6667</a></span><br/>
        <span>Copyright ⓒ GonnaJoong. 2021 All Rights Reserved</span>
    </address>
</footer>
